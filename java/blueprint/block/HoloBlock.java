package blueprint.block;

import blueprint.material.MaterialHolo;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class HoloBlock extends Block {
	public static final PropertyBool VISIBILITY = PropertyBool.create("visibility");
	static Material holo = new MaterialHolo(MapColor.airColor);
    public HoloBlock(String blockName,int id) {
    	super(holo);
    	GameRegistry.registerBlock(this, ItemHoloBlock.class, blockName);
    	setUnlocalizedName(blockName);
    	setCreativeTab(CreativeTabs.tabBlock);
    	setDefaultState(this.blockState.getBaseState().withProperty(VISIBILITY, true));
    }
    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
        System.out.println("inActivated");
        if (getMetaFromState(state) == 1) {
       		System.out.println("state: " + state + ", State from Meta: " + getStateFromMeta(0));
       		worldIn.setBlockState(pos, getStateFromMeta(0));
       		return true;
        }
        return false;
    }
    /**
     * Check whether this Block can be placed on the given side
     */
    /*
    @Override
    public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side) {
    	System.out.println(worldIn.getBlockState(pos).getBlock());
    	System.out.println(side);
    	if (side == side.UP) {
    		if (worldIn.getBlockState(pos.add(0, -1, 0)).getBlock().equals(this))
    			return true;
    	}
    	if (side.equals(side.DOWN)) {
    		if (worldIn.getBlockState(pos.add(0, 1, 0)).getBlock().equals(this))
				return true;
    	}
    	if (side == side.EAST) {
    		System.out.println("East: " + side);
    	}
    	if (side == side.WEST) {
    		System.out.println("West: " + side);
    	}
    	if (side == side.NORTH) {
    		System.out.println("North: " + side);
    	}
    	if (side == side.SOUTH) {
    		System.out.println("South: " + side);
    	}
        //if (worldIn.getBlockState(pos).getBlock().equals(this))
		return false;
        //return this.canPlaceBlockAt(worldIn, pos);
    }
    */
    /**
     * The type of render function called. 3 for standard block models, 2 for TESR's, 1 for liquids, -1 is no render
     */
    @Override
    public int getRenderType() {
        return 3;
    }
    @Override
    public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state) {
        return null;
    }
    
    /**
     * Used to determine ambient occlusion and culling when rebuilding chunks for render
     */
    @Override
    public boolean isOpaqueCube() {
        return false;
    }
    /**
     * If returns false, Block behaves like air.
     */
    @Override
    public boolean canCollideCheck(IBlockState state, boolean hitIfLiquid) {
    	//System.out.println("state: " + state);
        return true; //normalfall spieler ist nicht teil der struktur dann false
    }
    /**
     * Spawns this Block's drops into the World as EntityItems.
     *  
     * @param chance The chance that each Item is actually spawned (1.0 = always, 0.0 = never)
     * @param fortune The player's fortune level
     */
    @Override
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune) {}
    /*
    /**
     * 
     * @param world
     * @param pos
     * @return boolean True if BlockPos in question has neighbours that are neither air nor HoloBlock.
     */
    /*
    private boolean hasNeighbouringBlocks(World world, BlockPos pos) {
    	LinkedList<Block> blocks = new LinkedList<Block>();
    	blocks.add(world.getBlockState(pos.add(1, 0, 0)).getBlock());
    	blocks.add(world.getBlockState(pos.add(-1, 0, 0)).getBlock());
    	blocks.add(world.getBlockState(pos.add(0, 0, 1)).getBlock());
    	blocks.add(world.getBlockState(pos.add(0, 0, -1)).getBlock());
    	blocks.add(world.getBlockState(pos.add(0, 1, 0)).getBlock());
    	blocks.add(world.getBlockState(pos.add(0, -1, 0)).getBlock());
    	Iterator<Block> iterator = blocks.iterator();
    	while( iterator.hasNext()) {
    	    Block block = iterator.next();
    	    if (!block.equals(this) && !isEqualTo(block, Blocks.air))	
    	        return true;
    	}
    	return false;
    }
    */
    /**
    * Whether this Block can be replaced directly by other blocks (true for e.g. tall grass)
    */
    @Override
    public boolean isReplaceable(World worldIn, BlockPos pos){
    	if (getMetaFromState(worldIn.getBlockState(pos)) == 1) {// visibility is true (1)
    		// Player is Player from Structure from Block:
    		//if (worldIn.isRemote) {
    			//if (Structure.hasPlayer(Minecraft.getMinecraft().thePlayer)) {
    				//if (hasNeighbouringBlocks(worldIn, pos) == true) {
    					//System.out.println("Neighbour block exists");
    					return false; //Do changes in SetBlockQueue
    				//}
    			//}
    			//else {
    				//Print Player is not part of planned Structure. AND checkNeighbouringBlocks(worldIn, pos) == false.
    				//return false;
    			//}
    		//}
    				//System.out.println("no Neighbour block exists or Block is HoloBlock");
    				//return false;
    	}
    	return true;
    }
    //######### added Methods to air #########
    
	/**
	 * needed to make player destroy only block he is clicking, not blocks behind this block etc.
	 */
	@Override
	public boolean isFullBlock() {
		return true;
	}
    @SideOnly(Side.CLIENT) @Override
    public EnumWorldBlockLayer getBlockLayer()
    {
        return EnumWorldBlockLayer.TRANSLUCENT;
    }
	/**
	 * prevents vanilla digging particles
	 */
	@SideOnly(Side.CLIENT) @Override
    public boolean addHitEffects(World worldObj, MovingObjectPosition target, net.minecraft.client.particle.EffectRenderer effectRenderer)
    {
        return true;
    }
	/**
	 * prevents vanilla break particles
	 */
	@SideOnly(Side.CLIENT) @Override
    public boolean addDestroyEffects(World world, BlockPos pos, net.minecraft.client.particle.EffectRenderer effectRenderer)
    {
        return true;
    }
	@Override
	public boolean canSilkHarvest() {
		return false;
	}
    //######## BLOCKSTATE #########
    @Override
	public BlockState createBlockState() {
		return new BlockState(this, new IProperty[] {VISIBILITY});	
	}
	@Override
	public IBlockState getStateFromMeta(int meta) {
		if (meta == 0)
			return this.getDefaultState().withProperty(VISIBILITY, false);
		return this.getDefaultState().withProperty(VISIBILITY, true);
	}
	@Override
	public int getMetaFromState(IBlockState state) {
	    return (Boolean)(state.getValue(VISIBILITY)) ? 1 : 0;
	}
	@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing blockFaceClickedOn, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
		return getStateFromMeta(1);
	}
}
