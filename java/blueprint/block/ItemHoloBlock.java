package blueprint.block;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class ItemHoloBlock extends ItemBlock {
	public ItemHoloBlock(Block block) {
		super(block);
    	this.setMaxDamage(0);
	}
	@Override
	public int getMetadata(int metadata) {
		return metadata;
	}
}