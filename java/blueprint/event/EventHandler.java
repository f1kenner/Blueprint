package blueprint.event;

import blueprint.block.HoloBlock;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent.MultiPlaceEvent;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandler {
	/**
	 * used to avoid placing from blocks by right clicking HoloBlock with block
	 * as currentItem.
	 * 
	 * @param event
	 */
	@SubscribeEvent
	public void onBlockPlacedEvent(PlaceEvent event) {
		if (!HoloBlock.class.isInstance(event.placedBlock.getBlock()))
			if (HoloBlock.class.isInstance(event.placedAgainst.getBlock()))
				event.setCanceled(true);
	}

	@SubscribeEvent
	public void onBlockMultiPlacedEvent(MultiPlaceEvent event) {
		if (!HoloBlock.class.isInstance(event.placedBlock.getBlock()))
			if (HoloBlock.class.isInstance(event.placedAgainst.getBlock()))
				event.setCanceled(true);
	}

	/**
	 * used to avoid placing from chests, trapped chests, itemframes and
	 * paintings by clicking a HoloBlock
	 * 
	 * @param event
	 */
	@SubscribeEvent
	public void playerInteractEvent(PlayerInteractEvent event) {
		if (event.entityPlayer.inventory.getCurrentItem() != null) {
			if (event.action == Action.RIGHT_CLICK_BLOCK
					&& HoloBlock.class.isInstance(event.world.getBlockState(event.pos).getBlock())
					&& (event.entityPlayer.inventory.getCurrentItem().getItem()
							.equals(Item.getItemFromBlock(Blocks.chest))
							|| event.entityPlayer.inventory.getCurrentItem().getItem()
									.equals(Item.getItemFromBlock(Blocks.trapped_chest))
							|| event.entityPlayer.inventory.getCurrentItem().getItem().equals(Items.item_frame)
							|| event.entityPlayer.inventory.getCurrentItem().getItem().equals(Items.painting)))
				event.useItem = Result.DENY;
		}
	}
}
