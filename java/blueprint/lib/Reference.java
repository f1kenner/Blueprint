package blueprint.lib;

public class Reference {
	
	public static final String 
			MODID = "blueprint",
			NAME = "Blueprint",
			DEPENDENCIES = "",
			VERSION = "1.0",
			MINECRAFT = "[1.8,)";
	
	public static final String
		DIRT = "holoDirt",
		STONE = "holoStone",
		COBBLESTONE = "holoCobblestone",
		COAL = "holoCoal", 
		IRON = "holoIron", 
		LAPIS = "holoLapis", 
		GOLD = "holoGold", 
		REDSTONE = "holoRedstone",
		LITREDSTONE = "holoRedstone",
		EMERALD = "holoEmerald", 
		DIAMOND = "holoDiamond",
		QUARTZ = "holoQuartz";
}
