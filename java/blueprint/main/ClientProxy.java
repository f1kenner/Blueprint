package blueprint.main;

import blueprint.block.HoloBlocks;
import blueprint.lib.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		HoloBlocks.clientPreInit();
	}

	@Override
	public void init(FMLInitializationEvent event) {
		super.init(event);
		registerRenderer(HoloBlocks.dirt, Reference.DIRT);
		HoloBlocks.clientInit();
		
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}

	
	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return (ctx.side.isClient() ? Minecraft.getMinecraft().thePlayer : super.getPlayerEntity(ctx));
	}
	@Override
	public void registerRenderer(Block block, String modelName) {
		System.out.println("REGISTERING BLOCK RENDERER: " + modelName); 
		String tempString = Reference.MODID + ":" + modelName;
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), 0, new ModelResourceLocation(tempString, "inventory"));
	}
}