package blueprint.main;

import blueprint.block.HoloBlocks;
import blueprint.event.EventHandler;
import blueprint.gui.SchematicGui;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {
		HoloBlocks.commonInit();
	}

	public void init(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new EventHandler());
	}

	public void postInit(FMLPostInitializationEvent event) {
		//MinecraftForge.EVENT_BUS.register(new SchematicGui(Minecraft.getMinecraft()));
	}

	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return null;
	}

	public void registerRenderer(Block block, String modelName) {		
	}

}
