package blueprint.material;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class MaterialHolo extends Material {

	public MaterialHolo(MapColor color) {
		super(color);
		this.setImmovableMobility();
	}
	@Override
	public boolean blocksMovement() {
		return false;
	}
	@Override
	public boolean blocksLight() {
        return false;
    }
//	@Override
//	public boolean isReplaceable() {
//		return true;
//	}
}
