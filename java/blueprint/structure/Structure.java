package blueprint.structure;

import java.util.LinkedList;
import net.minecraft.entity.player.EntityPlayer;

public class Structure {

	private LinkedList<EntityPlayer> players = new LinkedList<EntityPlayer>();
	private LinkedList<Substructure> subStructures = new LinkedList<Substructure>();
	private String structureName="", subStructureName="main";
	// **** CONSTRUCTOR Structure(players, subStructures, structureName) *****
	public Structure (LinkedList<EntityPlayer> players, LinkedList<Substructure> subStructures, String structureName ) {
		this.setPlayers(players);
		subStructures.add(new Substructure(players, subStructures, structureName, subStructureName));
		this.setSubstructures(subStructures);
		this.setStructureName(structureName);
	}
	public void createSubstructure(String subStructureName) {
		subStructures.add(new Substructure(players, subStructures, structureName, subStructureName));
	}
	public void removeSubstructure(String subStructureName) {
		Substructure tempStruct = getSubstructure(subStructureName);
		if (tempStruct != null) {
			subStructures.remove(tempStruct);
		}
	}
	public void mergeSubstructure() {
		
	}	
	public void renameSubstructure(String oldName, String newName) {
		getSubstructure(oldName).setSubStructureName(newName);
	}
	public void addPlayer(EntityPlayer player) {
		players.add(player);
	}
	public void removePlayer(EntityPlayer player) {
		players.remove(player);
	}
	// **** HELPER METHODS **** //
	public Substructure getSubstructure(String subStructName) {
		for (Substructure subStruct : subStructures) {
			if (subStruct.getStructureName().equals(subStructName)) {
				return subStruct;
			}
		}
		return null;
	}
	// **** GETTERS & SETTERS *****
	public String getStructureName() {
		return structureName;
	}
	public void setStructureName(String structureName) {
		this.structureName = structureName;
	}
	public LinkedList<Substructure> getSubStructures() {
		return subStructures;
	}
	public void setSubstructures(LinkedList<Substructure> subStructures) {
		this.subStructures = subStructures;
	}
	public LinkedList<EntityPlayer> getPlayers() {
		return players;
	}
	public void setPlayers(LinkedList<EntityPlayer> players) {
		this.players = players;
	}
}
