package blueprint.structure;

import java.util.LinkedList;

import blueprint.block.HoloBlock;
import net.minecraft.entity.player.EntityPlayer;

public class Substructure extends Structure {
	private LinkedList<HoloBlock> setBlocks = new LinkedList<HoloBlock>();
	private String subStructureName;
	// **** CONSTRUCTOR SubStructure(players, subStructures, structureName, subStructureName) *****
	public Substructure(LinkedList<EntityPlayer> players, LinkedList<Substructure> subStructures,
			String structureName, String subStructureName) {
		super(players, subStructures, structureName);
	}
	// **** GETTERS & SETTERS *****
	public String getSubStructureName() {
		return subStructureName;
	}
	public void setSubStructureName(String subStructureName) {
		this.subStructureName = subStructureName;
	}
}
